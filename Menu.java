import java.util.Scanner;

public class Menu {

    public static void main(String[]args) {
        int opcao;

        do {
            System.out.println("|   *MENU*   |");
            System.out.println("| 1. Opção 1 |");
            System.out.println("| 2. Opção 2 |");
            System.out.println("|  3. Sair   |");
            //A linha abaixo é para dar espaçamento para ficar mais organizado no console
            System.out.println("");
            Scanner menu = new Scanner(System.in);

            System.out.print("Digite a opção desejada: ");
            opcao = menu.nextInt();

            switch (opcao) {

                case 1: {
                    System.out.println("Você escolheu a primeira opção.");
                    //A linha abaixo é para dar espaçamento para ficar mais organizado no console
                    System.out.println("");
                    break;
                }

                case 2: {
                    System .out.println("Você escolheu a segunda opção.");
                    //A linha abaixo é para dar espaçamento para ficar mais organizado no console
                    System.out.println("");
                    break;
                }

                case 3: {
                    System.out.println("O programa foi encerrado.");
                    //A linha abaixo é para dar espaçamento para ficar mais organizado no console
                    System.out.println("");
                    break;
                }

                default:
                    System.out.println("Opção invalida, selecione de 1 a 3");
                    //A linha abaixo é para dar espaçamento para ficar mais organizado no console
                    System.out.println("");
            } 
        } while (opcao != 3);
    }
}